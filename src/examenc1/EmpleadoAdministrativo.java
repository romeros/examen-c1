/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author BRANDON ROMERO
 */
public class EmpleadoAdministrativo extends Empleado {
    private int tipoContrato;

    public EmpleadoAdministrativo(int numEmpleado, String nombre, String domicilio, float pagoPorDia, int diasTrabajados, float impuestoISR, int tipoContrato) {
        super(numEmpleado, nombre, domicilio, pagoPorDia, diasTrabajados, impuestoISR);
        this.tipoContrato = tipoContrato;
    }
//get y setters
    
    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public float calcularRetencion() {
        float pagoBruto = calcularPagoBruto();
        if (pagoBruto < 5000) {
            return pagoBruto * 0.05f;
        } else if (pagoBruto <= 10000) {
            return pagoBruto * 0.08f;
        } else {
            return pagoBruto * 0.10f;
        }
    }

    @Override
    public float calcularPagoNeto() {
        return super.calcularPagoNeto() - calcularRetencion();
    }
}

