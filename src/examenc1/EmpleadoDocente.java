/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author BRANDON ROMERO
 */
public class EmpleadoDocente extends Empleado {
    private int nivel;

    public EmpleadoDocente(int numEmpleado, String nombre, String domicilio, float pagoPorDia, int diasTrabajados, float impuestoISR, int nivel) {
        super(numEmpleado, nombre, domicilio, pagoPorDia, diasTrabajados, impuestoISR);
        this.nivel = nivel;
    }

    
    //get y setters
    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float calcularPagoAdicional() {
        float pagoBruto = calcularPagoBruto();
        switch (nivel) {
            case 1: return pagoBruto * 0.35f;
            case 2: return pagoBruto * 0.40f;
            case 3: return pagoBruto * 0.50f;
            default: return 0;
        }
    }

    @Override
    public float calcularPagoNeto() {
        return super.calcularPagoBruto() + calcularPagoAdicional() - calcularDescuentoISR();
    }
}
