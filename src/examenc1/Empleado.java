/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author BRANDON ROMERO
 */
public class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected float pagoPorDia;
    protected int diasTrabajados;
    protected float impuestoISR;

    public Empleado(int numEmpleado, String nombre, String domicilio, float pagoPorDia, int diasTrabajados, float impuestoISR) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoPorDia = pagoPorDia;
        this.diasTrabajados = diasTrabajados;
        this.impuestoISR = impuestoISR;
    }

    
    //get y setter 
    
    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoPorDia() {
        return pagoPorDia;
    }

    public void setPagoPorDia(float pagoPorDia) {
        this.pagoPorDia = pagoPorDia;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public float getImpuestoISR() {
        return impuestoISR;
    }

    public void setImpuestoISR(float impuestoISR) {
        this.impuestoISR = impuestoISR;
    }

    public float calcularPagoBruto() {
        return pagoPorDia * diasTrabajados;
    }

    public float calcularDescuentoISR() {
        return calcularPagoBruto() * (impuestoISR / 100);
    }

    public float calcularPagoNeto() {
        return calcularPagoBruto() - calcularDescuentoISR();
    }
}
